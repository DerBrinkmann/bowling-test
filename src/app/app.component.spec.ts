import { AppComponent } from './app.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { BowlingGameComponent } from './components/bowling-game/bowling-game.component';
import { GameService } from './core/services/game.service';
import { BowlingFrameComponent } from './components/bowling-frame/bowling-frame.component';
import { ScorePipe } from './components/score.pipe';
import { CommonModule } from '@angular/common';
import { PlayersFormComponent } from './components/players-form/players-form.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  let gameService = new GameService();

  const createComponent = createComponentFactory({
    component: AppComponent,
    declarations: [BowlingFrameComponent, BowlingGameComponent, PlayersFormComponent, ScorePipe],
    providers: [{ provide: GameService, useValue: gameService }],
    imports: [CommonModule, ReactiveFormsModule],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  afterEach(() => {
    gameService.endGame();
  });

  it('should create', () => {
    expect(spectator).toBeDefined();
  });

  it('should show the headline', () => {
    expect(spectator.query('h1')).toHaveText('Bowling');
  });

  it('should initially show the playerForm', () => {
    const playerForm = spectator.query('app-players-form');
    expect(playerForm).toExist();
    expect(playerForm).toBeVisible();
    expect(spectator.query('app-bowling-game')).not.toBeVisible();
  });

  it('should show the game if the game has started', () => {
    gameService.newGame(['test']);
    spectator.detectChanges();
    expect(spectator.query('app-bowling-game')).toBeVisible();
  });
});
