import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { GameService } from './core/services/game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(public gameService: GameService) {}
}
