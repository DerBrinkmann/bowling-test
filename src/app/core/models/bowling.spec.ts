import { BowlingGame } from './bowling.model';

describe('Bowling Game Class', () => {
  it('should create the game with the defined number of players', () => {
    const game = new BowlingGame(['test', 'test2']);
    expect(game.players.length).toEqual(2);
  });

  it('should be 300 score for a perfect run', () => {
    const game = new BowlingGame(['test']);
    Array.from({ length: 12 }, () => 10).forEach((roll) => game.scorePoints(roll));
    expect(game.currentPlayer.currentFrame.completeScore).toEqual(300);
  });

  it('should calculate the correct game score', () => {
    const game = new BowlingGame(['test']);
    const rolls = [8, 1, 0, 9, 2, 8, 10, 6, 3, 7, 0, 5, 2, 10, 0, 6, 2, 8, 10];
    const expectedResult = 122;

    rolls.forEach((points) => {
      game.scorePoints(points);
    });

    expect(game.currentPlayer.currentFrame.isLastFrame).toBeTruthy();
    expect(game.currentPlayer.currentFrame.completeScore).toEqual(expectedResult);
  });
});
