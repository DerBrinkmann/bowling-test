export class BowlingGame {
  public players: BowlingPlayer[];
  public currentPlayer: BowlingPlayer;
  private activePlayerIndex = 0;

  constructor(playerNames: string[]) {
    this.players = playerNames.map((name) => new BowlingPlayer(name));
    this.currentPlayer = this.players[0] || null;
  }

  public scorePoints(points: number): void {
    const closedFrame = this.players[this.activePlayerIndex].addPointsToFrame(points);
    if (closedFrame) {
      this.activePlayerIndex = (this.activePlayerIndex + 1) % this.players.length;
      this.currentPlayer = this.players[this.activePlayerIndex];
    }
  }
}

export class BowlingPlayer {
  public name: string;
  public bowlingFrames: BowlingFrame[];
  public currentFrame: BowlingFrame;
  private currentFrameIndex = 0;
  private previousRoll?: Roll;

  constructor(name: string) {
    this.name = name;
    let previousFrame: BowlingFrame;
    this.bowlingFrames = Array.from({ length: 10 }, (_, index) => {
      const frame = index === 9 ? new LastFrame(previousFrame) : new BowlingFrame(previousFrame);
      previousFrame = frame;
      return frame;
    });
    this.currentFrame = this.bowlingFrames[0];
  }

  public addPointsToFrame(points: number): boolean {
    if (this.currentFrame.closed) {
      return true;
    }

    const roll = new Roll(points);
    if (this.previousRoll) {
      this.previousRoll.nextRoll = roll;
    }
    this.currentFrame.addRoll(roll);
    const frameClosed = this.currentFrame.closed;
    if (frameClosed && this.currentFrameIndex < this.bowlingFrames.length - 1) {
      this.currentFrameIndex++;
      this.currentFrame = this.bowlingFrames[this.currentFrameIndex];
    }
    this.previousRoll = roll;
    this.updateLastFrames();
    return frameClosed;
  }

  private updateLastFrames() {
    this.bowlingFrames.forEach((frame) => frame.updateCompleteScore());
  }
}

export class BowlingFrame {
  public rolls: Roll[] = [];
  public isLastFrame = false;
  public previousFrame?: BowlingFrame;
  public closed = false;
  public score: number = 0;
  public completeScore: number = 0;
  public pinsLeft = 10;

  constructor(previousFrame?: BowlingFrame) {
    this.previousFrame = previousFrame;
  }

  public addRoll(roll: Roll): void {
    if (roll.points > this.pinsLeft) {
      throw new Error('Invalid Throw');
      return;
    }

    const numberOfRolls = this.rolls.push(roll);
    this.score += roll.points;
    this.pinsLeft -= roll.points;
    if (numberOfRolls == 2) {
      roll.previousRollInFrame = this.rolls[0];
    }
    if (roll.points === 10 || numberOfRolls === 2) {
      this.closed = true;
    }
    this.updateCompleteScore();
  }

  public updateCompleteScore() {
    let score = 0;
    score += this.previousFrame?.completeScore || 0;
    score += this.rolls.reduce((result, roll) => {
      result += roll.points;
      if (roll.rollType === RollType.SPARE || roll.rollType == RollType.STRIKE) {
        result += roll.nextRoll?.points || 0;
        if (roll.rollType == RollType.STRIKE) {
          result += roll.nextRoll?.nextRoll?.points || 0;
        }
      }
      return result;
    }, 0);
    this.completeScore = score;
  }
}

export class LastFrame extends BowlingFrame {
  public override isLastFrame = true;

  constructor(previousFrame?: BowlingFrame) {
    super(previousFrame);
  }

  public override addRoll(roll: Roll) {
    if (roll.points > this.pinsLeft) {
      throw new Error('Invalid Throw');
      return;
    }

    if (this.rolls.length < 2 || (this.rolls.length === 2 && this.score >= 10)) {
      this.rolls.push(roll);
      this.score += roll.points;
    }
    if (this.rolls.length === 2 && this.score === 10) {
      this.pinsLeft = 10;
    }
    if ((this.rolls.length === 2 && this.score < 10) || this.rolls.length === 3) {
      this.closed = true;
    }

    this.updateCompleteScore();
  }

  public override updateCompleteScore() {
    this.completeScore = this.previousFrame?.completeScore + this.score;
  }
}

export enum RollType {
  GUTTER = 'GUTTER',
  HIT = 'HIT',
  STRIKE = 'STRIKE',
  SPARE = 'SPARE',
}

export class Roll {
  public points: number;
  public rollType: RollType;
  public nextRoll?: Roll;

  constructor(points: number) {
    this.points = points;
    if (points === 0) {
      this.rollType = RollType.GUTTER;
    } else if (points > 0 && points < 10) {
      this.rollType = RollType.HIT;
    } else if (points === 10) {
      this.rollType = RollType.STRIKE;
    }
  }

  public set previousRollInFrame(previousRoll: Roll) {
    if (previousRoll.rollType === RollType.HIT && previousRoll.points + this.points === 10) {
      this.rollType = RollType.SPARE;
    }
  }
}
