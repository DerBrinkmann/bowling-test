import { GameService } from './game.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';

describe('GameService', () => {
  let spectator: SpectatorService<GameService>;
  const createService = createServiceFactory(GameService);

  beforeEach(() => (spectator = createService()));

  it('should create', () => {
    expect(spectator).toBeDefined();
  });

  it('should have no Game', () => {
    expect(spectator.service.game).toBeUndefined();
  });

  it('should create a game for the players', () => {
    const players = ['test', 'test2'];
    spectator.service.newGame(players);
    expect(spectator.service.game).toBeDefined();
    expect(spectator.service.game.players.length).toEqual(players.length);
  });

  it('should delete the game object on end', () => {
    const players = ['test', 'test2'];
    spectator.service.newGame(players);
    spectator.service.endGame();
    expect(spectator.service.game).toBeNull();
  });
});
