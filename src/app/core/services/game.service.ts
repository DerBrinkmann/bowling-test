import { Injectable } from '@angular/core';
import { BowlingGame } from '../models/bowling.model';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  public game: BowlingGame;

  constructor() {}

  public newGame(players: string[]): void {
    this.game = new BowlingGame(players);
  }

  public restartGame(): void {
    if (this.game) {
      this.newGame(this.game.players.map((player) => player.name));
    }
  }

  public endGame(): void {
    this.game = null;
  }
}
