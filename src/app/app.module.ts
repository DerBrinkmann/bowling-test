import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BowlingFrameComponent } from './components/bowling-frame/bowling-frame.component';
import { ScorePipe } from './components/score.pipe';
import { BowlingGameComponent } from './components/bowling-game/bowling-game.component';
import { PlayersFormComponent } from './components/players-form/players-form.component';

@NgModule({
  declarations: [AppComponent, BowlingFrameComponent, ScorePipe, BowlingGameComponent, PlayersFormComponent],
  imports: [CommonModule, BrowserModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
