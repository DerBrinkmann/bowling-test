import { ScorePipe } from './score.pipe';
import { createPipeFactory, SpectatorPipe } from '@ngneat/spectator';
import { Roll } from '../core/models/bowling.model';

describe('ScorePipe', () => {
  let spectator: SpectatorPipe<ScorePipe>;
  const createPipe = createPipeFactory(ScorePipe);

  it('should return points if roll of type HIT', () => {
    spectator = createPipeWithRoll(new Roll(5));
    expect(spectator.element).toHaveText('5');
  });

  it('should return X if roll of type STRIKE', () => {
    spectator = createPipeWithRoll(new Roll(10));
    expect(spectator.element).toHaveText('X');
  });

  it('should return - if roll of type GUTTER', () => {
    spectator = createPipeWithRoll(new Roll(0));
    expect(spectator.element).toHaveText('-');
  });

  it('should return / if roll of type SPARE', () => {
    const roll = new Roll(5);
    roll.previousRollInFrame = new Roll(5);
    spectator = createPipeWithRoll(roll);
    expect(spectator.element).toHaveText('/');
  });

  function createPipeWithRoll(roll: Roll): SpectatorPipe<ScorePipe> {
    return createPipe(`{{roll | score}}`, {
      hostProps: { roll },
    });
  }
});
