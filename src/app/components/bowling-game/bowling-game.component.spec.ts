import { BowlingGameComponent } from './bowling-game.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CommonModule } from '@angular/common';
import { BowlingFrameComponent } from '../bowling-frame/bowling-frame.component';
import { ScorePipe } from '../score.pipe';
import { BowlingGame } from '../../core/models/bowling.model';

describe('BowlingGameComponent', () => {
  let spectator: Spectator<BowlingGameComponent>;

  const createComponent = createComponentFactory({
    component: BowlingGameComponent,
    declarations: [BowlingFrameComponent, ScorePipe],
    imports: [CommonModule],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  describe('single player', () => {
    const players = ['test'];
    let game: BowlingGame;

    beforeEach(() => {
      game = new BowlingGame(players);
      spectator.setInput('game', game);
      spectator.detectChanges();
    });

    it('should render the point buttons', () => {
      const rollBtns = spectator.queryAll('.roll-btn-row button');
      expect(rollBtns.length).toBe(11);
    });

    it('should add the score to the current frame and player', () => {
      expect(game.currentPlayer.currentFrame.rolls.length).toBe(0);
      clickPointsBtn(1);
      expect(game.currentPlayer.currentFrame.rolls.length).toBe(1);
      expect(game.currentPlayer.currentFrame.rolls[0].points).toBe(1);
    });

    it('should disable the buttons if they are greater than the leftover pins', () => {
      clickPointsBtn(5);
      expect(spectator.query(`.e2e--points-6`)).toBeDisabled();
      expect(spectator.query(`.e2e--points-7`)).toBeDisabled();
      expect(spectator.query(`.e2e--points-8`)).toBeDisabled();
      expect(spectator.query(`.e2e--points-9`)).toBeDisabled();
      expect(spectator.query(`.e2e--points-10`)).toBeDisabled();
    });
  });

  describe('multiple players', () => {
    const players = ['test', 'test2', 'test3'];
    let game: BowlingGame;

    beforeEach(() => {
      game = new BowlingGame(players);
      spectator.setInput('game', game);
      spectator.detectChanges();
    });

    it('should render the players', () => {
      const playerNames = spectator.queryAll('.player-name');
      expect(playerNames.length).toBeGreaterThan(0);
      playerNames.forEach((nameElement: Element, index: number) => {
        expect(nameElement.textContent).toBe(players[index]);
      });
    });
  });

  function clickPointsBtn(points: number) {
    const selector = `.e2e--points-${points}`;
    expect(spectator.query(selector)).toBeDefined();
    spectator.click(selector);
  }
});
