import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { BowlingGame } from '../../core/models/bowling.model';

@Component({
  selector: 'app-bowling-game',
  templateUrl: './bowling-game.component.html',
  styleUrls: ['./bowling-game.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BowlingGameComponent {
  @Input() game: BowlingGame;
  @Output() restart = new EventEmitter();
  @Output() end = new EventEmitter();

  public scoreButtons = Array.from({ length: 11 });

  constructor() {}
}
