import { Component, Input, OnInit } from '@angular/core';
import { BowlingFrame } from '../../core/models/bowling.model';

@Component({
  selector: 'app-bowling-frame',
  templateUrl: './bowling-frame.component.html',
  styleUrls: ['./bowling-frame.component.scss'],
})
export class BowlingFrameComponent implements OnInit {
  @Input() bowlingFrame: BowlingFrame;

  constructor() {}

  ngOnInit(): void {}
}
