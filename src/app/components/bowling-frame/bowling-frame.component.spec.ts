import { BowlingFrameComponent } from './bowling-frame.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { BowlingFrame, Roll } from '../../core/models/bowling.model';
import { ScorePipe } from '../score.pipe';

describe('BowlingFrameComponent', () => {
  let spectator: Spectator<BowlingFrameComponent>;

  const createComponent = createComponentFactory({ component: BowlingFrameComponent, declarations: [ScorePipe] });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator).toBeDefined();
  });

  it('should show the points of the frame', () => {
    const bowlingFrame = new BowlingFrame();
    bowlingFrame.addRoll(new Roll(4));
    bowlingFrame.addRoll(new Roll(5));
    spectator.setInput('bowlingFrame', bowlingFrame);
    spectator.detectChanges();
    expect(spectator.query('.e2e--frame-roll-1')).toHaveText('4');
    expect(spectator.query('.e2e--frame-roll-2')).toHaveText('5');
    expect(spectator.query('.score')).toHaveText('9');
  });
});
