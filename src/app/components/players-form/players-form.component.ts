import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Form, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-players-form',
  templateUrl: './players-form.component.html',
  styleUrls: ['./players-form.component.scss'],
})
export class PlayersFormComponent {
  @Output() startGame = new EventEmitter<string[]>();
  public form: FormArray;
  public maxLength = 6;

  constructor(private fb: FormBuilder) {
    this.form = fb.array([this.newPlayerFormControl()]);
  }

  public addPlayer(): void {
    this.form.push(this.newPlayerFormControl());
  }

  public onStartGame(): void {
    this.startGame.emit(this.form.value);
  }

  private newPlayerFormControl(): FormControl {
    return new FormControl('', [Validators.required]);
  }
}
