import { PlayersFormComponent } from './players-form.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

describe('PlayersFormComponent', () => {
  let spectator: Spectator<PlayersFormComponent>;
  const createComponent = createComponentFactory({
    component: PlayersFormComponent,
    imports: [CommonModule, ReactiveFormsModule],
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeDefined();
  });

  it('should initially render one input field', () => {
    const inputElements = spectator.queryAll('input[type="text"]');
    expect(inputElements.length).toBe(1);
  });

  it('should add a control and input on add button click', () => {
    spectator.click('.e2e--add-player');
    expect(spectator.component.form.length).toBe(2);

    const inputElements = spectator.queryAll('input[type="text"]');
    expect(inputElements.length).toBe(2);
  });

  it('should emit the value of the list on start button click', (done) => {
    const testName = 'test';
    spectator.typeInElement(testName, 'input[type="text"]');
    spectator.component.startGame.subscribe((event) => {
      expect(event.length).toBe(1);
      expect(event[0]).toBe(testName);
      done();
    });
    expect(spectator.component.form.value[0]).toBe(testName);
    spectator.click('.e2e--start-game');
  });
});
