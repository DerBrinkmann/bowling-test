import { Pipe, PipeTransform } from '@angular/core';
import { Roll, RollType } from '../core/models/bowling.model';

@Pipe({
  name: 'score',
})
export class ScorePipe implements PipeTransform {
  transform(roll: Roll): unknown {
    if (!roll) {
      return;
    }

    switch (roll.rollType) {
      case RollType.GUTTER:
        return '-';
      case RollType.HIT:
        return roll.points;
      case RollType.STRIKE:
        return 'X';
      case RollType.SPARE:
        return '/';
    }
  }
}
